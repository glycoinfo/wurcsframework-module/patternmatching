package org.glycoinfo.wurcsframework.patternmatching.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.wurcsframework.patternmatching.WURCSMatcher;
import org.glycoinfo.wurcsframework.patternmatching.pattern.PatternDictionary;
import org.glycoinfo.wurcsframework.patternmatching.util.DictionaryUtil;

public class TestWURCSMatcher {

	public static void main(String[] args) {
		File file = new File("src/test/resources/fromPubChem/wurcs_glycan_wurcs_2023Jun24.lst");
		if (!file.exists()) {
			System.err.print("File not exist.");
			return;
		}

		PatternDictionary dict = new PatternDictionary();
//		DictionaryUtil.setSNFGPatternAll(dict);
		DictionaryUtil.setSTORMPatternAll(dict);
//		DictionaryUtil.addSNFGPatternForMSNames(dict, "Glc,Gal,Man".split(","));
//		dict.addMSPattern("aGlcp", "a2122h-1a_1-5", "alpha-D-glucopyranose");
//		dict.addMSPattern("bGlcp", "a2122h-1b_1-5", "beta-D-glucopyranose");
		boolean allowUnknown = false;

		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int i=0, tot=0;
			String strWURCS;
			while ((strWURCS = bufferedReader.readLine()) != null) {
				try {
					WURCSMatcher matcher = new WURCSMatcher(dict);
					matcher.setDoCheckAnomer(false);
					matcher.setDoFuzzyMatch(true);
					matcher.start(strWURCS);
					boolean isMatch = matcher.getReport().hasMatchedPattern();
					if ( !allowUnknown )
						isMatch &= !matcher.getReport().hasUnknownPattern();
					if ( isMatch ) {
						System.out.println(strWURCS);
						System.out.println( matcher.getReport().getSummary() );
						i++;
					}
					tot++;
				} catch (WURCSFormatException e) {
					e.printStackTrace();
				}
			}
			bufferedReader.close();

			// Show results
			System.out.println();
			System.out.println(i+"/"+tot);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
