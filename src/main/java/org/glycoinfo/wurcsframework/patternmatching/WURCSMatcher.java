package org.glycoinfo.wurcsframework.patternmatching;

import java.util.ArrayList;
import java.util.List;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.util.subsumption.WURCSSubsumptionConverter;
import org.glycoinfo.WURCSFramework.wurcs.array.LIN;
import org.glycoinfo.WURCSFramework.wurcs.array.MOD;
import org.glycoinfo.WURCSFramework.wurcs.array.MODAbstract;
import org.glycoinfo.WURCSFramework.wurcs.array.MS;
import org.glycoinfo.WURCSFramework.wurcs.array.RES;
import org.glycoinfo.WURCSFramework.wurcs.array.UniqueRES;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.wurcsframework.patternmatching.pattern.AbstractPattern;
import org.glycoinfo.wurcsframework.patternmatching.pattern.MSPattern;
import org.glycoinfo.wurcsframework.patternmatching.pattern.PatternDictionary;
import org.glycoinfo.wurcsframework.patternmatching.util.PatternUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WURCSMatcher {

	private static Logger logger = LoggerFactory.getLogger(WURCSMatcher.class);

	private PatternDictionary dict;
	private WURCSMatcherReport report;

	private boolean doCheckAnomer;
	private boolean doFuzzyMatch;

	public WURCSMatcher(PatternDictionary a_dict) {
		this.dict = a_dict;
		this.report = new WURCSMatcherReport();

		this.doCheckAnomer = false;
		this.doFuzzyMatch = false;
	}

	public WURCSMatcherReport getReport() {
		return this.report;
	}

	public void setDoCheckAnomer(boolean a_doCheckAnomer) {
		this.doCheckAnomer = a_doCheckAnomer;
	}

	public void setDoFuzzyMatch(boolean a_doFuzzyMatch) {
		this.doFuzzyMatch = a_doFuzzyMatch;
	}

	public void start(String a_strWURCS) throws WURCSFormatException {
		WURCSImporter parser = new WURCSImporter();
		WURCSArray array = parser.extractWURCSArray(a_strWURCS);

		// Check URESs
		for ( UniqueRES ures : array.getUniqueRESs() ) {
			// Count RESs
			int nRES = 0;
			for ( RES res : array.getRESs() )
				if ( res.getUniqueRESID() == ures.getUniqueRESID() )
					nRES++;

			List<AbstractPattern> lMSPatterns = match(ures);
			this.report.addMSPatterns(lMSPatterns, nRES);
		}

		// Check LINs
		List<AbstractPattern> lLINPatterns = new ArrayList<>();
		for ( LIN lin : array.getLINs() )
			lLINPatterns.add( match(lin, false) );
		this.report.addPatterns(lLINPatterns, 1);
	}

	/**
	 * Return List of patterns matched to target MS. When two or more MSPatterns
	 * are matched, most matched one is selected. If target MS contains MODs
	 * which does not contained in the matched patterns, the MAPs are also matched
	 * by MAPPatterns in dictionary. The matched MAPPatterns are also added into
	 * the list of patterns to be returned.
	 * @param a_msTarget the target MS
	 * @return {@code true} if the target contains this pattern list
	 */
	public List<AbstractPattern> match(MS a_msTarget) {
		List<AbstractPattern> lMatchedPat = new ArrayList<>();

		// Convert MS to fuzzy state
		if ( this.doFuzzyMatch )
			a_msTarget = toFuzzyState(a_msTarget);

		// Check with MS patterns in dictionary
		List<MSPattern> lMatchedMSPat
			= this.dict.getMatchedMSPatterns(a_msTarget, this.doCheckAnomer);

		List<MOD> lMODs = a_msTarget.getMODs();
		if ( !lMatchedMSPat.isEmpty() ) {
			// Select first one as matched MSPattern
			MSPattern pat0 = lMatchedMSPat.get(0);
			lMatchedPat.add(pat0);

			// Extract unmatched MODs using diff()
			MS msDiff = PatternUtil.diff(a_msTarget, pat0.getMS());
			lMODs = msDiff.getMODs();
		}

		// Extract unmatched MODs with MAP pattern
		for ( MOD mod : lMODs )
			// Add MAPPatterns which matched to mod. null MAPPattern could be contained.
			lMatchedPat.add( match(mod, true) );

		return lMatchedPat;
	}

	public AbstractPattern match(MODAbstract a_modTarget, boolean a_onMonosaccharide) {
		String strMAP = a_modTarget.getMAPCode();
		// For empty MAP
		if ( strMAP.isEmpty() )
			return a_onMonosaccharide? AbstractPattern.RING : AbstractPattern.LINK;

		return this.dict.getMatchedMAPPattern(strMAP, a_onMonosaccharide);
	}

	private MS toFuzzyState(MS a_ms) {
		// Convert MS to unknown configuration, anomer and ring size
		// TODO: consider not to use WURCSSubsumptionConverter
		WURCSSubsumptionConverter conv = new WURCSSubsumptionConverter();
		MS msFuzzy = a_ms;
		MS msConv = conv.convertConfigurationToUnknown(msFuzzy);
		if ( msConv != null )
			msFuzzy = msConv;
		msConv = conv.convertUnknownAnomer(msFuzzy);
		if ( msConv != null )
			msFuzzy = msConv;
		msConv = conv.convertUnknownRingSize(msFuzzy);
		if ( msConv != null )
			msFuzzy = msConv;

		return msFuzzy;
	}
}
