package org.glycoinfo.wurcsframework.patternmatching.preset.storm;

import org.glycoinfo.wurcsframework.patternmatching.pattern.IMSNames;

public enum MSNamesSTORM_Pen implements IMSNames {

	// aldoses
	Pen("Penose", "axxxh-1x_1-?"),

	/// deoxy-/dideoxy-/trideoxy-
	_5dPen("5-Deoxy-pentose",            "axxxm-1x_1-?"),
	_2dPen("2-Deoxy-pentose",            "adxxh-1x_1-?"),
	_3dPen("3-Deoxy-pentose",            "axdxh-1x_1-?"),

	/// deoxy-amino-
	_2dPen4N("2,4-Dideoxy-4-amino-pentose", "adxxh-1x_1-?_4*N"),

	/// amino-
	Pen2N("2-Deoxy-2-amino-pentose", "axxxh-1x_1-?_2*N"),
	Pen4N("4-Deoxy-4-amino-pentose", "axxxh-1x_1-?_4*N"),

	/// multi-amino-
	Pen2N3N4N("2,3,4-Trideoxy-2,3,4-triamino-pentose", "axxxh-1x_1-5_2*N_3*N_4*N"),

	/// acetylated amino-
	Pen2NAc("2-Deoxy-2-N-acetyl-pentose", "axxxh-1x_1-?_2*NCC/3=O"),

	/// (di-)amino-aldonic acid
	Pen2N_onic("2-Deoxy-2-amino-pentonic acid", "Axxxh_2*N"),

	/// uronic acid
	PenA("penturonic acid", "axxxA-1x_1-?"),

	/// aldonic acid
	Pen_onic("pentonic acid", "Axxxh"),

	/// aldaric acid
	Pen_aric("pentaric acid", "AxxxA"),
	Pen2N3N_aric("pentaric acid", "AxxxA_2*N_3*N"),
	Pen4N5N_aric("pentaric acid", "AxxxA_4*N_5*N"),

	/// S-modified
	Pen5SH("6-deoxy-6-thio-pentose", "axxxh-1x_1-?_6*S"),


	// ketoses
	Pen2U("pent-2-ulose", "haxxh-2x_2-?"),

	/// ketoaldose
	Penos4U("pentos-4-ulose", "axxOh-1x_1-?"),


	// alditols
	Pen_ol("pentitol", "hxxxxh"),

	;

	private String m_strCommonName;
	private String m_strMS;

	private MSNamesSTORM_Pen(String a_strCommonName, String a_strMS) {
		this.m_strCommonName = a_strCommonName;
		this.m_strMS = a_strMS;
	}

	@Override
	public String getName() {
		String name = this.name();
		if ( name.startsWith("_") )
			name = name.substring(1);
		return name;
	}

	@Override
	public String getCommonName() {
		return this.m_strCommonName;
	}

	@Override
	public String getMSString() {
		return this.m_strMS;
	}

}
