package org.glycoinfo.wurcsframework.patternmatching.preset.storm;

import org.glycoinfo.wurcsframework.patternmatching.pattern.IMSNames;

public enum MSNamesSTORM_Hex implements IMSNames {

	// aldoses
	Hex("Hexose",            "axxxxh-1x_1-?"),

	/// deoxy-/dideoxy-/trideoxy-
	_6dHex("6-Deoxy-hexose",            "axxxxm-1x_1-?"),
	_2dHex("2-Deoxy-hexose",            "adxxxh-1x_1-?"),
	_3dHex("3-Deoxy-hexose",            "axdxxh-1x_1-?"),
	_4dHex("4-Deoxy-hexose",            "axxdxh-1x_1-?"),
	_2d6dHex("2,6-Dideoxy-hexose",      "adxxxm-1x_1-?"),
	_3d6dHex("3,6-Dideoxy-hexose",      "axdxxm-1x_1-?"),
	_4d6dHex("4,6-Dideoxy-hexose",      "axxdxm-1x_1-?"),
	_2d3d6dHex("2,3,6-Trideoxy-hexose", "addxxm-1x_1-?"),
	_3d4d6dHex("3,4,6-Trideoxy-hexose", "axddxm-1x_1-?"),
	_2d4d6dHex("2,4,6-Trideoxy-hexose", "adxdxm-1x_1-?"),

	/// deoxy-3-methyl-
	_6dHex3OMe("6-Deoxy-3-O-methyl-hexose", "axxxxm-1x_1-?_3*OC"),

	/// dideoxy-3-methyl-
	_2d6dHex3OMe("2,6-Dideoxy-3-O-methyl-hexose", "adxxxm-1x_1-?_3*OC"),
	_3d6dHex3OMe("3,6-Dideoxy-3-O-methyl-hexose", "axdxxm-1x_1-?_3*OC"),
	_4d6dHex3OMe("4,6-Dideoxy-3-O-methyl-hexose", "axxdxm-1x_1-?_3*OC"),

	/// deoxy-amino-
	_6dHex2N("2,6-Dideoxy-2-amino-hexose", "axxxxm-1x_1-?_2*N"),
	_6dHex3N("3,6-Dideoxy-3-amino-hexose", "axxxxm-1x_1-?_3*N"),
	_6dHex4N("4,6-Dideoxy-4-amino-hexose", "axxxxm-1x_1-?_4*N"),

	/// multi-deoxy-amino-
	_2d6dHex3N("2,3,6-Trideoxy-3-amino-hexose", "adxxxm-1x_1-?_3*N"),
	_2d6dHex4N("2,4,6-Trideoxy-4-amino-hexose", "adxxxm-1x_1-?_4*N"),
	_3d6dHex2N("2,3,6-Trideoxy-2-amino-hexose", "axdxxm-1x_1-?_2*N"),
	_3d6dHex4N("3,4,6-Trideoxy-4-amino-hexose", "axdxxm-1x_1-?_4*N"),
	_4d6dHex2N("2,4,6-Trideoxy-2-amino-hexose", "axxdxm-1x_1-?_2*N"),
	_4d6dHex3N("3,4,6-Trideoxy-3-amino-hexose", "axxdxm-1x_1-?_3*N"),

	_3d4dHex2N6N("2,3,4,6-Tetradeoxy-2,6-diamino-hexose", "axddxm-1x_1-?_2*N_6*N"),

	/// deoxy-diamino-
	_6dHex2N3N("2,3,6-Dideoxy-2,3-diamino-hexose", "axxxxm-1x_1-?_2*N_3*N"),
	_6dHex2N4N("2,4,6-Dideoxy-2,4-diamino-hexose", "axxxxm-1x_1-?_2*N_4*N"),

	/// (multi-)deoxy-dimethylamino-
	_6dHex3NMe2("3,6-Dideoxy-3-dimethylamino-hexose", "axxxxm-1x_1-?_3*NC/2C"),

	_2d6dHex3NMe2("2,3,6-Trideoxy-3-dimethylamino-hexose", "adxxxm-1x_1-?_3*NC/2C"),
	_4d6dHex3NMe2("3,4,6-Trideoxy-3-dimethylamino-hexose", "axxdxm-1x_1-?_3*NC/2C"),

	_2d3d6dHex4NMe2("2,3,4,6-Tetradeoxy-3-dimethylamino-hexose", "addxxm-1x_1-?_4*NC/2C"),

	/// acetylated deoxy-amino-
	_6dHex2NAc("2,6-Dideoxy-2-N-acetyl-hexose", "axxxxm-1x_1-?_2*NCC/3=O"),

	/// amino-
	Hex2N("2-Deoxy-2-amino-hexose", "axxxxh-1x_1-?_2*N"),
	Hex3N("3-Deoxy-3-amino-hexose", "axxxxh-1x_1-?_3*N"),
	Hex4N("4-Deoxy-4-amino-hexose", "axxxxh-1x_1-?_4*N"),
	Hex6N("6-Deoxy-6-amino-hexose", "axxxxh-1x_1-?_6*N"),

	/// multi-amino-
	Hex2N3N("2,3-Dideoxy-2,3-diamino-hexose", "axxxxh-1x_1-?_2*N_3*N"),
	Hex2N6N("2,6-Dideoxy-2,6-diamino-hexose", "axxxxh-1x_1-?_2*N_6*N"),
	Hex3N4N("3,4-Dideoxy-3,4-diamino-hexose", "axxxxh-1x_1-?_3*N_4*N"),
	Hex3N6N("3,6-Dideoxy-3,6-diamino-hexose", "axxxxh-1x_1-?_3*N_6*N"),

	/// acetylated amino-
	Hex2NAc("2-Deoxy-2-N-acetyl-hexose", "axxxxh-1x_1-?_2*NCC/3=O"),

	/// amino-uronic acid
	Hex2NA("2-Deoxy-2-amino-hexuronic acid", "axxxxA-1x_1-?_2*N"),
	Hex3NA("3-Deoxy-3-amino-hexuronic acid", "axxxxA-1x_1-?_3*N"),

	Hex2NAcA("2-Deoxy-2-N-acetyl-hexuronic acid", "axxxxA-1x_1-?_2*NCC/3=O"),

	/// diamino-uronic acid
	Hex2N3NA("2,3-Deoxy-2,3-diamino-hexuronic acid", "axxxxA-1x_1-?_2*N_3*N"),
	Hex2N4NA("2,4-Deoxy-2,4-diamino-hexuronic acid", "axxxxA-1x_1-?_2*N_4*N"),

	/// (di-)amino-aldonic acid
	Hex2N_onic("2-Deoxy-2-amino-hexonic acid", "Axxxxh_2*N"),

	Hex4N5N_onic("4,5-Dideoxy-4,5-diamino-hexonic acid", "Axxxxh_4*N_5*N"),

	/// uronic acid
	HexA("hexuronic acid", "axxxxA-1x_1-?"),

	/// aldonic acid
	Hex_onic("hexonic acid", "Axxxxh"),

	/// aldaric acid
	Hex_aric("hexaric acid", "AxxxxA"),
	Hex2N3N_aric("hexaric acid", "AxxxxA_2*N_3*N"),
	Hex4N5N_aric("hexaric acid", "AxxxxA_4*N_5*N"),

	/// S-modified
	Hex6S("6-deoxy-6-C-sulpho-hexose", "axxxxh-1x_1-?_6*SO/2=O/2=O"),


	// ketoses
	Hex2U("hex-2-ulose", "haxxxh-2x_2-?"),

	/// derivatives
	_6dHex2U("6-deoxy-hex-2-ulose", "haxxxm-2x_2-?"),
	_1d6dHex2U("1,6-dideoxy-hex-2-ulose", "maxxxm-2x_2-?"),

	Hex2U_onic("hex-2-ulosonic acid", "AOxxxh"), // TODO: open-chain only

	/// ketoaldose
	Hexos2U("hexos-2-ulose", "aOxxxh-1x_1-?"),
	Hexos3U("hexos-3-ulose", "axOxxh-1x_1-?"),
	Hexos4U("hexos-4-ulose", "axxOxh-1x_1-?"),

	_6dHexos3U("6-deoxy-hexos-3-ulose", "axOxxm-1x_1-?"),
	_6dHexos4U("6-deoxy-hexos-4-ulose", "axxOxm-1x_1-?"),

	_2d6dHexos3U("2,6-deoxy-hexos-3-ulose", "adOxxm-1x_1-?"),
	_3d6dHexos4U("3,6-deoxy-hexos-4-ulose", "axdOxm-1x_1-?"),

	/// amino-ketoaldose
	Hexos3U2N("2-Deoxy-2-amino-hexos-3-ulose", "axOxxh-1x_1-?_2*N"),

	_6dHexos4U2N("2,6-Dideoxy-2-amino-hexos-4-ulose", "axxOxm-1x_1-?_2*N"),


	// alditols
	Hex_ol("hexitol", "hxxxxh"),

	/// deoxy-alditol
	_6dHex_ol("6-deoxy-hexitol", "hxxxxm"),

	/// amino-alditol
	Hex2N_ol("2-Deoxy-2-amino-hexitol", "hxxxxh_2*N"),
	Hex5N_ol("5-Deoxy-5-amino-hexitol", "hxxxxh_5*N"),

	Hex2NAc_ol("2-Deoxy-2-amino-hexitol", "hxxxxh_2*NCC/3=O"),
	Hex5NAc_ol("2-Deoxy-5-amino-hexitol", "hxxxxh_5*NCC/3=O"),

	/// deoxy-amino-alditol
	_6dHex1N_ol("1,6-Dideoxy-1-amino-hexitol", "hxxxxm_1*N"),
	_6dHex3N_ol("3,6-Dideoxy-3-amino-hexitol", "hxxxxm_3*N"),
	_6dHex4N_ol("4,6-Dideoxy-4-amino-hexitol", "hxxxxm_4*N"),

	;

	private String m_strCommonName;
	private String m_strMS;

	private MSNamesSTORM_Hex(String a_strCommonName, String a_strMS) {
		this.m_strCommonName = a_strCommonName;
		this.m_strMS = a_strMS;
	}

	@Override
	public String getName() {
		String name = this.name();
		if ( name.startsWith("_") )
			name = name.substring(1);
		return name;
	}

	@Override
	public String getCommonName() {
		return this.m_strCommonName;
	}

	@Override
	public String getMSString() {
		return this.m_strMS;
	}

}
