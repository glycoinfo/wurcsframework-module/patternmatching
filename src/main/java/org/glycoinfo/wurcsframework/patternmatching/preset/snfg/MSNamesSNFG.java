package org.glycoinfo.wurcsframework.patternmatching.preset.snfg;

import org.glycoinfo.wurcsframework.patternmatching.pattern.IMSNames;

public enum MSNamesSNFG implements IMSNames {

	Hex("Hexose",      "axxxxh-1x_1-5"),
	Glc("D-Glucose",   "a2122h-1x_1-5"),
	Man("D-Mannose",   "a1122h-1x_1-5"),
	Gal("D-Galactose", "a2112h-1x_1-5"),
	Gul("D-Gulose",    "a2212h-1x_1-5"),
	Alt("L-Altrose",   "a2111h-1x_1-5"),
	All("D-Allose",    "a2222h-1x_1-5"),
	Tal("D-Talose",    "a1112h-1x_1-5"),
	Ido("L-Idose",     "a2121h-1x_1-5"),

	HexNAc("N-Acetyl-hexosamine",      "axxxxh-1x_1-5_2*NCC/3=O"),
	GlcNAc("N-Acetyl-D-glucosamine",   "a2122h-1x_1-5_2*NCC/3=O"),
	ManNAc("N-Acetyl-D-mannosamine",   "a1122h-1x_1-5_2*NCC/3=O"),
	GalNAc("N-Acetyl-D-galactosamine", "a2112h-1x_1-5_2*NCC/3=O"),
	GulNAc("N-Acetyl-D-gulosamine",    "a2212h-1x_1-5_2*NCC/3=O"),
	AltNAc("N-Acetyl-L-altrosamine",   "a2111h-1x_1-5_2*NCC/3=O"),
	AllNAc("N-Acetyl-D-allosamine",    "a2222h-1x_1-5_2*NCC/3=O"),
	TalNAc("N-Acetyl-D-talosamine",    "a1112h-1x_1-5_2*NCC/3=O"),
	IdoNAc("N-Acetyl-L-idosamine",     "a2121h-1x_1-5_2*NCC/3=O"),

	HexN("Hexosamine",      "axxxxh-1x_1-5_2*N"),
	GlcN("D-Glucosamine",   "a2122h-1x_1-5_2*N"),
	ManN("D-Mannosamine",   "a1122h-1x_1-5_2*N"),
	GalN("D-Galactosamine", "a2112h-1x_1-5_2*N"),
	GulN("D-Gulosamine",    "a2212h-1x_1-5_2*N"),
	AltN("L-Altrosamine",   "a2111h-1x_1-5_2*N"),
	AllN("D-Allosamine",    "a2222h-1x_1-5_2*N"),
	TalN("D-Talosamine",    "a1112h-1x_1-5_2*N"),
	IdoN("L-Idosamine",     "a2121h-1x_1-5_2*N"),

	HexA("Hexuronate",          "axxxxA-1x_1-5"),
	GlcA("D-Glucuronic acid",   "a2122A-1x_1-5"),
	ManA("D-Mannuronic acid",   "a1122A-1x_1-5"),
	GalA("D-Galacturonic acid", "a2112A-1x_1-5"),
	GulA("D-Guluonic acid",     "a2212A-1x_1-5"),
	AltA("L-Altruronic acid",   "a2111A-1x_1-5"),
	AllA("D-Alluronic acid",    "a2222A-1x_1-5"),
	TalA("D-Taluronic acid",    "a1112A-1x_1-5"),
	IdoA("L-Iduronic acid",     "a2121A-1x_1-5"),

	dHex  ("Deoxyhexose",       "axxxxm-1x_1-5"),
	Qui   ("D-Quinovose",       "a2122m-1x_1-5"),
	Rha   ("L-Rhamnose",        "a2211m-1x_1-5"),
	_6dGul("6-Deoxy-D-gulose",  "a2212m-1x_1-5"),
	_6dAlt("6-Deoxy-L-altrose", "a2111m-1x_1-5"),
	_6dTal("6-Deoxy-D-talose",  "a1112m-1x_1-5"),
	Fuc   ("L-Fucose",          "a1221m-1x_1-5"),

	dHexNAc  ("DeoxyhexNAc",                    "axxxxm-1x_1-5_2*NCC/3=O"),
	QuiNAc   ("N-Acetyl-D-quinovosamine",       "a2122m-1x_1-5_2*NCC/3=O"),
	RhaNAc   ("N-Acetyl-L-rhamnosamine",        "a2211m-1x_1-5_2*NCC/3=O"),
	_6dAltNAc("N-Acetyl-6-deoxy-L-altrosamine", "a2111m-1x_1-5_2*NCC/3=O"),
	_6dTalNAc("N-Acetyl-6-deoxy-D-talosamine",  "a1112m-1x_1-5_2*NCC/3=O"),
	FucNAc   ("N-Acetyl-L-fucosamine",          "a1221m-1x_1-5_2*NCC/3=O"),

	ddHex("Di-deoxyhexose", "adxxxm-1x_1-5"),
	Oli  ("Olivose",        "ad122m-1x_1-5"),
	Tyv  ("Tyvelose",       "a1d22m-1x_1-5"),
	Abe  ("Abequose",       "a2d12m-1x_1-5"),
	Par  ("Pratose",        "a2d22m-1x_1-5"),
	Dig  ("D-Digitoxose",   "ad222m-1x_1-5"),
	Col  ("Colitose",       "a1d21m-1x_1-5"),

	Pen("Pentose",          "axxxh-1x_1-5"),
	Ara("L-Arabinose",      "a211h-1x_1-5"),
	Lyx("D-Lyxose",         "a112h-1x_1-5"),
	Xyl("D-Xylose",         "a212h-1x_1-5"),
	Rib("D-Ribose",         "a222h-1x_1-5"),

	NulO  ("Deoxynonulosonate",         "Aadxxxxxh-2x_2-6"),
	Kdn   ("3-Deoxy-D-glycero-D-galacto-nonulosonic Acid", "Aad21122h-2x_2-6"),
	Neu5Ac("N-Acetylneuraminic acid",   "Aad21122h-2x_2-6_5*NCC/3=O"),
	Neu5Gc("N-Glycolylneuraminic acid", "Aad21122h-2x_2-6_5*NCCO/3=O"),
	Neu   ("Neuraminic acid",           "Aad21122h-2x_2-6_5*N"),

	ddNulO("Di-deoxynonulosonate",   "Aadxxxxxm-2x_2-6_5*N_7*N"),
	Pse   ("Pseudaminic acid",       "Aad22111m-2x_2-6_5*N_7*N"),
	Leg   ("Legionaminic acid",      "Aad21122m-2x_2-6_5*N_7*N"),
	Aci   ("Acinetaminic acid",      "Aad21111m-2x_2-6_5*N_7*N"),
	_4eLeg("4-Epilegionaminic acid", "Aad11122m-2x_2-6_5*N_7*N"),

	Bac     ("Bacillosamine",          "a2122m-1x_1-5_2*N_4*N"),
	LDmanHep("L-glycero-D-manno-Heptose", "a11221h-1x_1-5"),
	Kdo     ("3-Deoxy-D-manno-octulosonic acid", "Aad1122h-2x_2-6"),
	Dha     ("3-Deoxy-D-lyxo-heptulosaric acid", "Aad112A-2x_2-6"),
	DDmanHep("D-glycero-D-manno-Heptose", "a11222h-1x_1-5"),
	MurNAc  ("N-Acetylmuramic acid",   "a2122h-1x_1-5_2*NCC/3=O_3*OC^RCO/4=O/3C"),
	MurNGc  ("N-Glycolylmuramic acid", "a2122h-1x_1-5_2*NCCO/3=O_3*OC^RCO/4=O/3C"),
	Mur     ("Muramic acid",           "a2122h-1x_1-5_2*N_3*OC^RCO/4=O/3C"),

	DApi("D-Apiose",  "a26h-1x_1-4_3*CO"), // SNFG says L-Api is default but it seems wrong 
	LApi("L-Apiose",  "a15h-1x_1-4_3*CO"),
	Fru("D-Fructose", "ha122h-2x_2-6"),
	Tag("D-Tagatose", "ha112h-2x_2-6"),
	Sor("L-Sorbose",  "ha121h-2x_2-6"),
	Psi("D-Psicose",  "ha222h-2x_2-6"),

	;

	private String m_strCommonName;
	private String m_strMS;

	private MSNamesSNFG(String a_strCommonName, String a_strMS) {
		this.m_strCommonName = a_strCommonName;
		this.m_strMS = a_strMS;
	}

	@Override
	public String getName() {
		String name = this.name();
		if ( name.startsWith("_") )
			name = name.substring(1);
		return name;
	}

	@Override
	public String getCommonName() {
		return this.m_strCommonName;
	}

	@Override
	public String getMSString() {
		return this.m_strMS;
	}
}
