package org.glycoinfo.wurcsframework.patternmatching.preset.storm;

import org.glycoinfo.wurcsframework.patternmatching.pattern.IMSNames;

public enum MSNamesSTORM_Hept_Atipical implements IMSNames {

	// Heptoses
	/// aldoses
	Hept("Heptose", "axxxxxh-1x_1-?"),

	/// deoxy-
	_6dHept("6-Deoxy-heptose", "axxxxdh-1x_1-?"),

	/// ketoses
	Hept2U("Hept-2-ulose", "axxxxxh-1x_1-?"),
	_6dHept4U("6-Deoxy-heptos-4-ulose", "haxOxdh-1x_1-?"),


	// Atipical
	/// Mur and derivatives
	Hex2N3OCetR("2-Deoxy-2-amino-3-(1R)-1-carboxyethyl-hexose", "axxxxh-1x_1-?_2*N_3*OC^RCO/4=O/3C"),
	Hex2NAc3OCetR("2-Deoxy-2-N-acetyl-3-(1R)-1-carboxyethyl-hexose", "axxxxh-1x_1-?_2*NCC/3=O_3*OC^RCO/4=O/3C"),
	Hex2NGc3OCetR("2-Deoxy-2-N-glycolyl-3-(1R)-1-carboxyethyl-hexose", "axxxxh-1x_1-?_2*NCCO/3=O_3*OC^RCO/4=O/3C"),

	/// iMur
	Hex2N3OCetS("2-Deoxy-2-amino-3-(1S)-1-carboxyethyl-hexose", "axxxxh-1x_1-?_2*N_3*OC^SCO/4=O/3C"),

	/// 1,6anhMur
	_16anhHex2N3OCetR("1,6-Anhydro-2-deoxy-2-amino-3-(1R)-1-carboxyethyl-hexose", "axxxxh-1x_1-?_1-6_2*N_3*OC^RCO/4=O/3C"),

	/// anhydro hex and hex-ol
	_16anhHex("1,6-Anhydro-hexose", "axxxxh-1x_1-?_1-6"),
	_36anhHex("3,6-Anhydro-hexose", "axxxxh-1x_1-?_3-6"),
	_25anhHex("2,5-Anhydro-hexose", "axxxxh-1x_1-?_2-5"),
	_15anhHex_ol("1,5-Anhydro-hexitol", "hxxxxh_1-5"),
	_25anhHex_ol("2,5-Anhydro-hexitol", "hxxxxh_2-5"),

	/// 2daraHexA
	_2dHexA("2-Deoxy-hexulonic acid", "adxxxA-1x_1-?"),

	/// Gal3F
	Hex3F("3-Fluoro-hexose", "axxxxh-1x_1-?_3*F"),

	/// GalN3N4NA
	Hex2N3N4NA("2.3.4-Trideoxy-2,3,4-triamino-hexulonic acid", "axxxxA-1x_1-?_2*N_3*N_4*N"),

	/// Mycinose
	_6dHex2OMe3OMe("6-Deoxy-2-O-methyl-3-O-methyl-hexose", "axxxxm-1x_1-?_2*OC_3*OC"),

	/// Javose
	_6dHex2OMe("6-Deoxy-2-O-methyl-hexose", "axxxxm-1x_1-?_2*OC"),

	/// Kdo-ol
	_3dOct_ol("3-Deoxy-octitol", "hxdxxxxh"),

	/// Mur-ol
	Hex2N3OCetR_ol("2-Deoxy-2-amino-3-(1R)-1-carboxyethyl-hexitol", "hxxxxh_2*N_3*OC^RCO/4=O/3C"),

	/// en-
	_4dHex4en("4-Deoxy-hex-4-enose", "axxzZh-1x_1-?"),
	_4dHex4enA("4-Deoxy-hex-4-enuronic acid", "axxzZA-1x_1-?"),
	_4dHex4en2NA("4-Deoxy-2-amino-hex-4-enuronic acid", "axxzZm-1x_1-?_2*N"),

	/// D-gro-D-3dGalNon5N-ulo
	_3dHex2U5N("3,5-Deoxy-5-amino-hex-2-ulose", "hadxxxh-2x_2-?_5*N"),

	/// Cinerulose A
	_2d3d6dHexos4U("2,3,6-Trideoxy-hexos-4-ulose", "addOxm-1x_1-5"),

	/// Aculose
	_6dHexos4U23en("6-Deoxy-hexos-4-ulo-2,3-enose", "azzOxm-1x_1-5"),

	/// 6daraHexos3ulo diacetal --- structure is uncertain
	//_6dHexos3U("???", "???"),
	;

	private String m_strCommonName;
	private String m_strMS;

	private MSNamesSTORM_Hept_Atipical(String a_strCommonName, String a_strMS) {
		this.m_strCommonName = a_strCommonName;
		this.m_strMS = a_strMS;
	}

	@Override
	public String getName() {
		String name = this.name();
		if ( name.startsWith("_") )
			name = name.substring(1);
		return name;
	}

	@Override
	public String getCommonName() {
		return this.m_strCommonName;
	}

	@Override
	public String getMSString() {
		return this.m_strMS;
	}

}
