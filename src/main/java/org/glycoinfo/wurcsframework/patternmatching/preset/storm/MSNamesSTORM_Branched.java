package org.glycoinfo.wurcsframework.patternmatching.preset.storm;

import org.glycoinfo.wurcsframework.patternmatching.pattern.IMSNames;

public enum MSNamesSTORM_Branched implements IMSNames {

	// apiose
	Tet3CHme("3-C-Hydroxymethyl-tetrose", "axXh-1x_1-4_3*CO"),

	// apiitol
	Tet3CHme_ol("3-C-Hydroxymethyl-tetritol", "hxXh-1x_3*CO"),

	// hamamelose
	Pen2CHme("2-C-Hydroxymethyl-pentose", "aXxxh-1x_1-?_2*CO"),

	// diH-OH-Streptose
	Pen3CHme("3-C-Hydroxymethyl-pentose", "axXxh-1x_1-?_3*CO"),

	// Amicolose
	_2d6dHex3CSEtN("2,6-Dideoxy-3-C-(1S)-1-aminoethyl-hexose", "adXxxm-1x_1-?_3*C^SN/3C"),
	_2d6dHex3CREtN("2,6-Dideoxy-3-C-(1R)-1-aminoethyl-hexose", "adXxxm-1x_1-?_3*C^RN/3C"),

	// Man3CMe
	Hex3CMe("3-C-Methyl-hexose", "axXxxh-1x_1-?_3*C"),

	// Aceric acid
	_5dPen3CA("5-Deoxy-3-C-carboxy-pentose", "axXxm-1x_1-4_3*CO/3=O"),

	// Streptose
	_5dPen3CFo("5-Deoxy-3-C-formyl-pentose", "axXxm-1x_1-4_3*C=O"),

	// Mycarose, Axenose
	_2d6dHex3CMe("2,6-Dideoxy-3-C-methyl-hexose", "adXxxm-1x_1-?_3*C"),

	// Evalose
	_6dHex3CMe("6-Deoxy-3-C-methyl-hexose", "axXxxm-1x_1-?_3*C"),

	// Garosamine
	Pen4CMe3NMe("3-Deoxy-4-C-methyl-3-N-methyl-pentose", "axxXh-1x_1-?_3*NC_4*C"),

	// Cradinose, Arcanose
	_2d6dHex3CMe3OMe("2,6-Dideoxy-3-C-methyl-3-O-methyl-hexose", "adXxxm-1x_1-?_3*C_3*OC"),

	// Elemosamine
	_2d6dHex3N3CMe("2,3,6-Trideoxy-3-amino-3-C-methyl-hexose", "adXxxm-1x_1-?_3*N_3*C"),

	// Kansosamine
	_6dHex2OMe3CMe4N("4,6-Dideoxy-2-O-methyl-3-C-methyl-4-amino-hexose", "axXxxm-1x_1-?_2*OC_3*C_4*N"),

	// Noviose
	_6dHex5CMe4OMe("6-Deoxy-5-C-methyl-4-O-methyl-hexose", "axXxxm-1x_1-?_4*OC_5*C"),

	// Trioxacarcinose B
	_2d6dHex4CAc("2,6-Dideoxy-4-C-acetyl-hexose", "adxXxm-1x_1-?_4*CC/2=O"),

	// Yersiniose (Yersinose A)	or Isoyersiniose (Yersinose B)
	_3d6dHex4CHetS("3,6-Dideoxy-4-C-(1S)-1-hydroxyethyl-hexose", "axdXxm-1x_1-?_4*C^SC/2O"),
	_3d6dHex4CHetR("3,6-Dideoxy-4-C-(1R)-1-hydroxyethyl-hexose", "axdXxm-1x_1-?_4*C^RC/2O"),

	// Paulomycose
	_2d6dHex4CHetS("2,6-Dideoxy-4-C-(1S)-1-hydroxyethyl-hexose", "adxXxm-1x_1-?_4*C^SC/2O"),
	_2d6dHex4CHetR("2,6-Dideoxy-4-C-(1R)-1-hydroxyethyl-hexose", "adxXxm-1x_1-?_4*C^RC/2O"),

	// Shewanellose
	_6dHex2NAc4C_Et2O2Cbm_("2,6-Dideoxy-2-N-acetyl-4-C-(2-carbamoyl-2carbonylethyl)-hexose", "axxXxm-1x_1-?_2*NCC/3=O_4*CCCN/4=O/3=O"),

	// Caryose --- failed due to the carbon cyclic
	_48cyc3d9dNon5CMe("4,8-cyclo-3,9-dideoxy-5-C-methyl-nonose", "axdxXxxx-1x_1-8_4-8**_5*C"),

	// Erwiniose
	_3d6d8dOct4CHetR("3,6,8-trideoxy-4-C-(1R)-1-hydroxymethyl-octose", "axdXxm-1x_1-5_4*C^SCC^RC/2O/4O"),

	// Caryophyllose
	_3d6d10dDec4CHetR("3,6,10-trideoxy-4-C-(1R)-hydroxyethyl-decose", "axdXxm-1x_1-5_4*C^SCC^RC^RC^RC/6O/5O/4O/2O"),

	// bradyrhizose --- failed due to the carbon cyclic
	_49cyc6dNon8CMe("4,9-cyclo-6-deoxy-8-C-methyl-nonose", "axxXxXxdx-1x_1-9_4-9**_6*C"),

	;

	private String m_strCommonName;
	private String m_strMS;

	private MSNamesSTORM_Branched(String a_strCommonName, String a_strMS) {
		this.m_strCommonName = a_strCommonName;
		this.m_strMS = a_strMS;
	}

	@Override
	public String getName() {
		String name = this.name();
		if ( name.startsWith("_") )
			name = name.substring(1);
		return name;
	}

	@Override
	public String getCommonName() {
		return this.m_strCommonName;
	}

	@Override
	public String getMSString() {
		return this.m_strMS;
	}

}
