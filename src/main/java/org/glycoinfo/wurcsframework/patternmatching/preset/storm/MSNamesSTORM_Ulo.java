package org.glycoinfo.wurcsframework.patternmatching.preset.storm;

import org.glycoinfo.wurcsframework.patternmatching.pattern.IMSNames;

public enum MSNamesSTORM_Ulo implements IMSNames {

	// Dha
	_3dHept2U_aric("3-Deoxy-heptulosaric acid", "AadxxxA-2x_2-?"),

	// Ko (ketooctonic)
	Oct2U_onic("oct-2-ulosonic acid", "Aaxxxxxh-2x_2-?"),

	// 3,7dKo (ketooctonic)
	_3d7dOct2U_onic("3,7-Dideoxy-oct-2-ulosonic acid", "Aadxxxdh-2x_2-?"),

	// Kdo (ketodeoxyoctonic)
	_3dOct2U_onic("3-Deoxy-oct-2-ulosonic acid", "Aadxxxxh-2x_2-?"),

	// Kdo8N
	_3dOct2U8N_onic("3,8-Dideoxy-8-amino-oct-2-ulosonic acid", "Aadxxxxh-2x_2-?_8*N"),

	// anhydro Kdos
	_27anh3dOct2U_onic("2,7-Anhydro-3-deoxy-oct-2-ulosonic acid", "Aadxxxdh-2x_2-?_2-7"),
	_47anh3dOct2U_onic("4,7-Anhydro-3-deoxy-oct-2-ulosonic acid", "Aadxxxdh-2x_2-?_4-7"),
	_48anh3dOct2U_onic("4,8-Anhydro-3-deoxy-oct-2-ulosonic acid", "Aadxxxdh-2x_2-?_4-8"),

	// Kdn (ketodeoxynononic)
	_3dNon2U_onic("3-Deoxy-non-2-ulosonic acid", "Aadxxxxxh-2x_2-?"),

	// Neu (neuraminic), Rhodaminic
	_3dNon2U5N_onic("3,5-Dideoxy-5-amino-non-2-ulosonic acid", "Aadxxxxxh-2x_2-?_5*N"),

	// Neu derivatives
	_3dNon2U5NAc_onic("3,5-Dideoxy-5-N-acetyl-non-2-ulosonic acid", "Aadxxxxxh-2x_2-?_5*NCC/3=O"),
	_3dNon2U5NGc_onic("3,5-Dideoxy-5-N-glycolyl-non-2-ulosonic acid", "Aadxxxxxh-2x_2-?_5*NCCO/3=O"),

	// dideoxy-amino-Kdns
	_3d9dNon2U5N7N_onic("3,5,7,9-Tetradeoxy-5,7-diamino-non-2-ulosonic acid", "Aadxxxxxm-2x_2-?_5*N_7*N"),
	_3d9dNon2U5N7N8N_onic("3,5,7,8,9-Pentadeoxy-5,7,8-triamino-non-2-ulosonic acid", "Aadxxxxxm-2x_2-?_5*N_7*N_8*N"),

	;

	private String m_strCommonName;
	private String m_strMS;

	private MSNamesSTORM_Ulo(String a_strCommonName, String a_strMS) {
		this.m_strCommonName = a_strCommonName;
		this.m_strMS = a_strMS;
	}

	@Override
	public String getName() {
		String name = this.name();
		if ( name.startsWith("_") )
			name = name.substring(1);
		return name;
	}

	@Override
	public String getCommonName() {
		return this.m_strCommonName;
	}

	@Override
	public String getMSString() {
		return this.m_strMS;
	}

}
