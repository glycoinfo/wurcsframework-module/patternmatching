package org.glycoinfo.wurcsframework.patternmatching.preset.snfg;

import org.glycoinfo.wurcsframework.patternmatching.pattern.IMAPNames;

public enum MAPNamesSNFG implements IMAPNames {
	OAc    ("O-acetyl",                "*OCC/3=O"),
	OAla   ("O-D-alanyl",              "*OCC^RC/4N/3=O"),
	OAla2Ac("O-(N-acetyl-D-alanyl)",   "*OCC^RNCC/6=O/4C/3=O"),
	Am     ("N-acetimidoyl",           "*N=^XCC/3N"),
	AmMe   ("N-acetimidoyl-N-methyl",  "*N=^XCC/3N/2C"),
	AmMe2  ("N-(N,N-dimethyl-acetimidoyl)", "*N=^XCNC/4C/3C"),
	OFo    ("O-formyl",                "*OC=O"),
	OGc    ("O-glycoryl",              "*OCCO/3=O"),
	OGln2Ac("O-(N-acetyl-glutaminyl)", "*OCC^XNCC/6=O/4CCCN/11=O/3=O"),
	O5Glu2Me("O-(N-methyl-5-glutamyl)", "*OCCCC^XCN/7=O/6NC/3=O"),
	OGly   ("O-glycyl",                "*OCCN/3=O"),
	OGr    ("O-glyceryl",              "*OCC^XCO/4O/3=O"),
	OGr2_3Me2("O-(2,3-di-O-methyl-glycaryl)", "*OCC^XCOC/4OC/3=O"),
	O4Hb   ("O-4-hydroxybutyryl",      "*OCCCCO/3=O"),
	O3_4Hb ("O-(3,4-dihydroxybutyryl)", "*OCCC^XCO/5O/3=O"),
	O3RHb  ("O-(R)-3-hydroxybutyryl",  "*OCCC^RC/5O/3=O"),
	O3SHb  ("O-(S)-3-hydroxybutyryl",  "*OCCC^SC/5O/3=O"),
	OLt    ("O-lactyl",                "*OCC^XC/4O/3=O"),
	OMe    ("O-methyl",                "*OC"),
	N      ("amino",                   "*N"),
	NAc    ("N-acetyl",                "*NCC/3=O"),
	NGc    ("N-glycoryl",              "*NCCO/3=O"),
	NS     ("N-sulfate",               "*NSO/3=O/3=O"),
	P      ("phosphate",               "*OPO/3O/3=O"),
	OPy    ("O-pyruvyl",               "*OCCC/4=O/3=O"),
	Pyr    ("1-carboxyethylidene",     "*OC^XO*/3CO/6=O/3C"),
	S      ("sulfate",                 "*OSO/3=O/3=O"),
	Tau    ("tauryl",                  "*NCCCSO/6=O/6=O"),

	// No definition in SNFG but required
	OMeOH  ("O-methoxy",               "*OCO"),
	OEt    ("O-ethyl",                 "*OCC"),
	OEtN   ("O-aminoethyl",            "*OCCN"),
	OEtOH  ("O-hydroxyethyl",          "*OCCO"),
	OEtOMe ("O-methoxyethyl",          "*OCCOC"),
	OPr    ("O-propyl",                "*OCCC"),
	NMe    ("N-methyl",                "*NC"),
	NMe2   ("N-dimethyl",              "*NC/2C"),
	NEt    ("N-ethyl",                 "*NCC"),
	NFo    ("N-formyl",                "*NC=O"),
	SH     ("thio",                    "*S"),
	SMe    ("S-methyl",                "*SC"),
	SEt    ("S-ethyl",                 "*SCC"),
	CS     ("C-sulfate",               "*SO/3=O/3=O"),
	OREtA  ("O-(1R)-1-carboxyethyl",   "*OC^RCO/4=O/3C"),
	PP     ("pyrophosphate",           "*OPOPO/5O/5=O/3O/3=O"),
	PPP    ("triphosphate",            "*OPOPOPO/7O/7=O/5O/5=O/3O/3=O"),

	F      ("fluolo",                  "*F"),
	Br     ("bromo",                   "*Br"),
	I      ("iodo",                    "*I"),
	;

	private String m_strCommonName;
	private String m_strMAP;

	private MAPNamesSNFG(String a_strCommonName, String a_strMAP) {
		this.m_strCommonName = a_strCommonName;
		this.m_strMAP = a_strMAP;
	}

	@Override
	public String getName() {
		String name = this.name();
		if ( name.contains("_") )
			name = name.replace('_', ',');
		return name;
	}

	@Override
	public String getCommonName() {
		return this.m_strCommonName;
	}

	@Override
	public String getMAPString() {
		return this.m_strMAP;
	}
}
