package org.glycoinfo.wurcsframework.patternmatching.util;

import org.glycoinfo.wurcsframework.patternmatching.pattern.PatternDictionary;
import org.glycoinfo.wurcsframework.patternmatching.preset.snfg.MAPNamesSNFG;
import org.glycoinfo.wurcsframework.patternmatching.preset.snfg.MSNamesSNFG;
import org.glycoinfo.wurcsframework.patternmatching.preset.storm.MSNamesSTORM_Branched;
import org.glycoinfo.wurcsframework.patternmatching.preset.storm.MSNamesSTORM_Hept_Atipical;
import org.glycoinfo.wurcsframework.patternmatching.preset.storm.MSNamesSTORM_Hex;
import org.glycoinfo.wurcsframework.patternmatching.preset.storm.MSNamesSTORM_Pen;
import org.glycoinfo.wurcsframework.patternmatching.preset.storm.MSNamesSTORM_Ulo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DictionaryUtil {

	private static Logger logger = LoggerFactory.getLogger(DictionaryUtil.class);

	public static void setSNFGPatternAll(PatternDictionary dict) {
		// load default MSPatterns
		for ( MSNamesSNFG names : MSNamesSNFG.values() )
			dict.addMSNames(names);
		for ( MAPNamesSNFG names : MAPNamesSNFG.values() )
			dict.addMAPNames(names);
	}

	public static void  addSNFGPatternForMSNames(PatternDictionary dict, String... a_names) {
		for ( String name : a_names )
			addSNFGPatternForMSName(dict, name);
	}

	private static void addSNFGPatternForMSName(PatternDictionary dict, String a_name) {
		for ( MSNamesSNFG names : MSNamesSNFG.values() )
			if ( names.getName().equals(a_name) ) {
				dict.addMSNames(names);
				return;
			}
		logger.warn("No MS pattern with the name <"+a_name+"> in SNFG.");
	}

	public static void  addSNFGPatternForMAPNames(PatternDictionary dict, String... a_names) {
		for ( String name : a_names )
			addSNFGPatternForMAPName(dict, name);
	}

	private static void addSNFGPatternForMAPName(PatternDictionary dict, String a_name) {
		for ( MAPNamesSNFG names : MAPNamesSNFG.values() )
			if ( names.getName().equals(a_name) ) {
				dict.addMAPNames(names);
				return;
			}
		logger.warn("No MAP pattern with the name <"+a_name+"> in SNFG.");
	}

	public static void setSTORMPatternAll(PatternDictionary dict) {
		// load STORM MSPatterns
		for ( MSNamesSTORM_Hex names : MSNamesSTORM_Hex.values() )
			dict.addMSNames(names);
		for ( MSNamesSTORM_Pen names : MSNamesSTORM_Pen.values() )
			dict.addMSNames(names);
		for ( MSNamesSTORM_Branched names : MSNamesSTORM_Branched.values() )
			dict.addMSNames(names);
		for ( MSNamesSTORM_Ulo names : MSNamesSTORM_Ulo.values() )
			dict.addMSNames(names);
		for (MSNamesSTORM_Hept_Atipical names : MSNamesSTORM_Hept_Atipical.values())
			dict.addMSNames(names);
		for ( MAPNamesSNFG names : MAPNamesSNFG.values() )
			dict.addMAPNames(names);
	}

}
