package org.glycoinfo.wurcsframework.patternmatching.util;

import org.glycoinfo.WURCSFramework.util.array.comparator.MODComparator;
import org.glycoinfo.WURCSFramework.wurcs.array.MOD;
import org.glycoinfo.WURCSFramework.wurcs.array.MS;

public class PatternUtil {

	/**
	 * Returns {@code true} if the source MS equals target MS.
	 * @param a_msSource The source MS
	 * @param a_msTarget The target MS
	 * @param a_bCheckAnomer flag to check anomer info
	 * @return {@code true} if the source MS equals target MS
	 * @see #contains(MS, MS)
	 */
	public static boolean equals(MS a_msSource, MS a_msTarget, boolean a_bCheckAnomer) {
		// Check if source and target MSs are contained each other
		return contains(a_msSource, a_msTarget, a_bCheckAnomer)
			&& contains(a_msTarget, a_msSource, a_bCheckAnomer);
	}

	/**
	 * Returns {@code true} if the source MS contains target MS.
	 * @param a_msSource The source MS
	 * @param a_msTarget The target MS
	 * @param a_bCheckAnomer flag to check anomer info
	 * @return {@code true} if the source MS contains target MS
	 * @see #diff(MS, MS)
	 */
	public static boolean contains(MS a_msSource, MS a_msTarget, boolean a_bCheckAnomer) {
		// Use diff() to check if source MS has elements not contained in target
		MS msDiff = diff(a_msSource, a_msTarget);
		if ( !msDiff.getSkeletonCode().isEmpty() )
			return false;
		if ( a_bCheckAnomer ) {
			if ( msDiff.getAnomericPosition() != -2 )
				return false;
			if ( msDiff.getAnomericSymbol() != '_' )
				return false;
		}
		if ( !msDiff.getMODs().isEmpty() )
			return false;
		return true;
	}

	/**
	 * Return MS only with differences between source and target MSs.
	 * The differences contain SkeletonCode if it differs between source and target,
	 * and MODs which are contained in source but not in target.
	 * @param a_msSource The source MS
	 * @param a_msTarget The target MS
	 * @return MS only with differences between source and target MSs
	 */
	public static MS diff(MS a_msSource, MS a_msTarget) {
		// Check differences of SkeletonCode and anomer between source and target
		String strSkeletonCode = a_msSource.getSkeletonCode();
		if ( strSkeletonCode.equals( a_msTarget.getSkeletonCode() ) )
			strSkeletonCode = "";
		int iAnomPos = a_msSource.getAnomericPosition();
		if ( iAnomPos == a_msTarget.getAnomericPosition() )
			iAnomPos = -2;
		char cAnom = a_msSource.getAnomericSymbol();
		if ( cAnom == a_msTarget.getAnomericSymbol() )
			cAnom = '_';
		// Construct MS only with differences
		MS msDiff = new MS(strSkeletonCode, iAnomPos, cAnom);

		// Add source MODs which not contained in target MS
		MODComparator comp = new MODComparator();
		for ( MOD modSource : a_msSource.getMODs() ) {
			boolean contains = false;
			for ( MOD modTarget : a_msTarget.getMODs() ) {
				if ( comp.compare(modSource, modTarget) == 0 )
					contains = true;
			}
			if ( !contains )
				msDiff.addMOD(modSource);
		}
		return msDiff;
	}

}
