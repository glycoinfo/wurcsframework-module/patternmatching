package org.glycoinfo.wurcsframework.patternmatching;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glycoinfo.wurcsframework.patternmatching.pattern.AbstractPattern;
import org.glycoinfo.wurcsframework.patternmatching.pattern.MAPPattern;
import org.glycoinfo.wurcsframework.patternmatching.pattern.MSPattern;

public class WURCSMatcherReport {

	private Map<String, Integer> mapMatchedMSNameToCount;
	private Map<String, Integer> mapMatchedMAPNameToCount;
	private int iRingCount;
	private int iLinkCount;
	private int iUnknownMSCount;
	private int iUnknownMAPCount;

	public WURCSMatcherReport() {
		this.mapMatchedMSNameToCount = new HashMap<>();
		this.mapMatchedMAPNameToCount = new HashMap<>();
		this.iRingCount = 0;
		this.iLinkCount = 0;
		this.iUnknownMSCount = 0;
		this.iUnknownMAPCount = 0;
	}

	public void addMSPatterns(List<AbstractPattern> a_lPatterns, int count) {
		MSPattern patMS = null;
		if ( !a_lPatterns.isEmpty() && a_lPatterns.get(0) instanceof MSPattern )
			patMS = (MSPattern)a_lPatterns.remove(0);
		// Count MS name
		addMSPattern(patMS, count);
		addPatterns(a_lPatterns, count);
	}

	public void addPatterns(List<AbstractPattern> a_lPatterns, int count) {
		for ( AbstractPattern pat : a_lPatterns ) {
			if ( pat == AbstractPattern.RING ) {
				addRingPattern(count);
				continue;
			}
			if ( pat == AbstractPattern.LINK ) {
				addLinkPattern(count);
				continue;
			}
			addMAPPattern((MAPPattern)pat, count);
		}
	}

	public void addMSPattern(MSPattern a_pattern, int count) {
		if ( a_pattern == null ) {
			iUnknownMSCount += count;
			return;
		}
		addNameCount(this.mapMatchedMSNameToCount, a_pattern.getName(), count);
	}

	public void addMAPPattern(MAPPattern a_pattern, int count) {
		if ( a_pattern == null ) {
			iUnknownMAPCount += count;
			return;
		}
		addNameCount(this.mapMatchedMAPNameToCount, a_pattern.getName(), count);
	}

	public void addRingPattern(int count) {
		this.iRingCount += count;
	}

	public void addLinkPattern(int count) {
		this.iLinkCount += count;
	}

	public boolean hasMatchedPattern() {
		return ( hasMatchedMSPattern() || hasMatchedMAPPattern() );
	}

	public boolean hasMatchedMSPattern() {
		return ( !this.mapMatchedMSNameToCount.isEmpty() );
	}

	public boolean hasMatchedMAPPattern() {
		return ( !this.mapMatchedMAPNameToCount.isEmpty() );
	}

	public boolean hasRingPattern() {
		return ( this.iRingCount > 0 );
	}

	public boolean hasLinkPattern() {
		return ( this.iLinkCount > 0 );
	}

	public boolean hasUnknownPattern() {
		return ( hasUnknownMSPattern() || hasUnknownMAPPattern() );
	}

	public boolean hasUnknownMSPattern() {
		return ( this.iUnknownMSCount > 0 );
	}

	public boolean hasUnknownMAPPattern() {
		return ( this.iUnknownMAPCount > 0 );
	}

	public String getSummary() {
		StringBuilder sb = new StringBuilder();

		for ( String name : this.mapMatchedMSNameToCount.keySet() )
			addInfo(sb, name, this.mapMatchedMSNameToCount.get(name));

		if ( this.iRingCount > 0 )
			addInfo(sb, "Ring", this.iRingCount);

		if ( this.iUnknownMSCount > 0 )
			addInfo(sb, "UndefMS", this.iUnknownMSCount);

		for ( String name : this.mapMatchedMAPNameToCount.keySet() )
			addInfo(sb, name, this.mapMatchedMAPNameToCount.get(name));

		if ( this.iLinkCount > 0 )
			addInfo(sb, "Link", this.iLinkCount);

		if ( this.iUnknownMAPCount > 0 )
			addInfo(sb, "UndefMAP", this.iUnknownMAPCount);

		return sb.toString();
	}

	private static void addInfo(StringBuilder sb, String name, int count) {
		if ( sb.length() != 0 )
			sb.append(",");
		sb.append(name);
		sb.append(":");
		sb.append(count);
	}

	private static void addNameCount(Map<String, Integer> a_mapNameToCount, String a_name, int a_count) {
		if ( a_mapNameToCount.containsKey(a_name) )
			a_count += a_mapNameToCount.get(a_name);
		a_mapNameToCount.put(a_name, a_count);
	}
}
