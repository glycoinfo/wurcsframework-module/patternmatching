package org.glycoinfo.wurcsframework.patternmatching.pattern;

import org.glycoinfo.WURCSFramework.wurcs.array.MS;

public class MSPattern extends AbstractPattern {

	private MS m_ms;

	MSPattern(MS a_ms, String a_strName, String a_strDesc) {
		super(a_strName, a_strDesc);
		this.m_ms = a_ms;
	}

	public MS getMS() {
		return this.m_ms;
	}
}
