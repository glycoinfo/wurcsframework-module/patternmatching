package org.glycoinfo.wurcsframework.patternmatching.pattern;

public abstract class AbstractPattern {

	/** Pattern for anhydro or ester rings */
	public static final AbstractPattern RING
		= new AbstractPattern("Ring", "Anhydro/Ester ring"){};
	/** Pattern for Glycosidic linkages */
	public static final AbstractPattern LINK
		= new AbstractPattern("Link", "Glycosidic linkage"){};

	private String m_strName;
	private String m_strDesc;

	AbstractPattern(String a_strName, String a_strDesc) {
		this.m_strName = a_strName;
		this.m_strDesc = a_strDesc;
	}

	public String getName() {
		return this.m_strName;
	}

	public String getDescription() {
		return this.m_strDesc;
	}

	public String toString() {
		return this.getName();
	}
}
