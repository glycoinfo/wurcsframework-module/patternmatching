package org.glycoinfo.wurcsframework.patternmatching.pattern;

public interface IMAPNames {

	public String getName();
	public String getCommonName();
	public String getMAPString();
}
