package org.glycoinfo.wurcsframework.patternmatching.pattern;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.util.array.comparator.MODComparator;
import org.glycoinfo.WURCSFramework.wurcs.array.MOD;
import org.glycoinfo.WURCSFramework.wurcs.array.MS;
import org.glycoinfo.wurcsframework.patternmatching.util.DictionaryUtil;
import org.glycoinfo.wurcsframework.patternmatching.util.PatternUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PatternDictionary {

	private static Logger logger = LoggerFactory.getLogger(DictionaryUtil.class);

	private Map<String, MSPattern> mapNameToMSPattern;
	private Map<String, MAPPattern> mapNameToMAPPattern;

	public PatternDictionary() {
		this.mapNameToMAPPattern = new HashMap<>();
		this.mapNameToMSPattern = new HashMap<>();
	}

	public boolean addMSNames(IMSNames a_names) {
		return addMSPattern(a_names.getName(), a_names.getMSString(), a_names.getCommonName());
	}

	/**
	 * Add MS pattern as a string of ResidueCode with the name and description.
	 * If a new pattern is added with the same name as an already added pattern,
	 * the already added pattern will be overwritten.
	 * @param a_strName Name for the MS pattern
	 * @param a_strMS ResidueCode of the MS pattern
	 * @param a_strDesc Description
	 * @return {@code true} if the addition is succeeded.
	 *  {@code false} if the {@code a_strMS} can not be parsed correctly.
	 */
	public boolean addMSPattern(String a_strName, String a_strMS, String a_strDesc) {
		try {
			MS ms = new WURCSImporter().extractMS(a_strMS);
			this.mapNameToMSPattern.put(a_strName, new MSPattern(ms, a_strName, a_strDesc) );
			return true;
		} catch (WURCSFormatException e) {
			logger.error("The MS pattern can not be added: {}", a_strMS);
			return false;
		}
		
	}

	public Collection<String> getMSPatternNames() {
		return this.mapNameToMSPattern.keySet();
	}

	public MSPattern getMSPattern(String a_name) {
		return this.mapNameToMSPattern.get(a_name);
	}

	public boolean hasMSPatternName(String a_name) {
		return this.mapNameToMSPattern.containsKey(a_name);
	}

	/**
	 * Get MSPatterns which matched to specified MS and conditions.
	 * When the {@code a_doCheckAnomer} is {@code false},
	 * anomer info of MSPatterns in the dictionary will be ignored.
	 * The matched MSPatterns are sorted by the matching condition that
	 * the more matched features, the more prioritized.
	 * @param a_msTarget MS to be matched
	 * @param a_doAllowSubstituents flag for allowing extra substituents
	 * @param a_doCheckAnomer flag for using anomer info
	 * @return List of matched MSPatterns
	 */
	public List<MSPattern> getMatchedMSPatterns(MS a_msTarget, boolean a_doCheckAnomer) {
		// Check with MS patterns in dictionary
		List<MSPattern> lMatchedPat = new ArrayList<>();
		for ( String strPatName : this.mapNameToMSPattern.keySet() ) {
			MSPattern pat = this.mapNameToMSPattern.get(strPatName);
			boolean isMatched = PatternUtil.contains(pat.getMS(), a_msTarget, a_doCheckAnomer);
			if ( !isMatched )
				continue;
			lMatchedPat.add(pat);
		}

		// Sort when two or more patterns
		if ( lMatchedPat.size() > 1 )
			lMatchedPat.sort(new Comparator<MSPattern>() {
				@Override
				public int compare(MSPattern ms1, MSPattern ms2) {
					int iComp = 0;
					// Check only MODs
					List<MOD> lMODs1 = ms1.getMS().getMODs();
					List<MOD> lMODs2 = ms2.getMS().getMODs();

					// Prior ones with large # of MODs
					iComp = lMODs2.size() - lMODs1.size();
					if ( iComp != 0 )
						return iComp;

					// Sort MODs of each MS
					MODComparator compMOD = new MODComparator();
					Collections.sort(lMODs1, compMOD);
					Collections.sort(lMODs2, compMOD);

					// Comp each MOD
					for ( int i=0; i<lMODs1.size(); i++ ) {
						iComp = compMOD.compare(lMODs1.get(i), lMODs2.get(i));
						if ( iComp != 0 )
							return iComp;
					}
					return 0;
				}
			});

		return lMatchedPat;
	}


	public boolean addMAPNames(IMAPNames a_names) {
		return addMAPPattern(a_names.getName(), a_names.getMAPString(), a_names.getCommonName());
	}

	public boolean addMAPPattern(String a_strName, String a_strMAP, String a_strDesc) {
		this.mapNameToMAPPattern.put(a_strName, new MAPPattern(a_strMAP, a_strName, a_strDesc));
		return true;
	}

	public Collection<String> getMAPNames() {
		return this.mapNameToMAPPattern.keySet();
	}

	public MAPPattern getMAPPatternForName(String a_name) {
		return this.mapNameToMAPPattern.get(a_name);
	}

	public MAPPattern getMatchedMAPPattern(String a_strMAP, boolean a_onMonosaccharide) {
		// Search MAPPattern with MAP
		for ( MAPPattern pat : this.mapNameToMAPPattern.values() )
			if ( pat.getMAP().equals(a_strMAP) )
				return pat;
		return null;
	}
}
