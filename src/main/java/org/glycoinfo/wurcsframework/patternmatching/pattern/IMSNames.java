package org.glycoinfo.wurcsframework.patternmatching.pattern;

public interface IMSNames {

	public String getName();
	public String getCommonName();
	public String getMSString();
}
