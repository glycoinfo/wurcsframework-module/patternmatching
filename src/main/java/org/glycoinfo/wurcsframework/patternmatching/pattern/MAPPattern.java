package org.glycoinfo.wurcsframework.patternmatching.pattern;

public class MAPPattern extends AbstractPattern {

	private String m_strMAP;

	MAPPattern(String a_strMAP, String a_strName, String a_strDesc) {
		super(a_strName, a_strDesc);
		this.m_strMAP = a_strMAP;
	}

	public String getMAP() {
		return this.m_strMAP;
	}
}
